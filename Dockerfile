FROM node:alpine
RUN apk add --no-cache tini
ENV install_dir /src/app
COPY . ${install_dir}
WORKDIR ${install_dir}

RUN \
  chown -R node.node ${install_dir} && \
  echo Ready

RUN \
  npm install --production && npm run build && npm cache -f clean \
  && rm -rf \
    /tmp/npm* \
  && echo Done

USER node
ENTRYPOINT ["/sbin/tini","--","node","server.js"]
