    sleep = (timeout) -> new Promise (resolve) -> setTimeout resolve, timeout

    describe 'The server', ->
      it 'should load', ->
        main = require '../server'
        process.env.CONFIG = JSON.stringify {}
        {sub,connection} = main true
        await sleep 1000
        sub.close()
        connection.socket.end()
