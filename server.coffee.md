    AMQP = require 'amqp'
    Axon = require '@shimaore/axon'
    most = require 'most'
    {NOTIFY} = require 'red-rings/operations'
    Config = require 'ccnq4-config'

    main = (test) ->
      config = Config()

Modeled after red-rings-axon/backend

      sub = Axon.socket 'sub'
      sub.bind config.sub

      sub.on 'error', (e) ->
        console.error 'Error from axon', e
        return

      key_match = new RegExp config.key_match

      source = most
        .fromEvent 'message', sub
        .filter ({op}) -> op is NOTIFY
        .filter ({key}) -> key.match key_match
        .multicast()

AMQP client

      connection = AMQP.createConnection config.amqp, reconnect: not test

      config.exchange ?=
        name: 'ccnq.out'
        type: 'direct'
        passive: false
        durable: true
        autoDelete: false
        noDeclare: false
        confirm: false

      connection.on 'error', (e) ->
        console.error 'Error from amqp', e
        return

      connection.on 'ready', ->
        console.error 'Ready from amqp'
        connection.exchange config.exchange.name, config.exchange, (exchange) ->

          console.error 'Exchange from amqp', config.exchange

Forward messages from the source to AMQP

          source
          .until most.fromEvent 'close', exchange
          .forEach (msg) ->

            rkey = msg.value?.event
            if rkey?
              exchange.publish rkey, msg,
                deliveryMode: 1 # non-persistent

            rkey = msg.value?.state
            if rkey?
              exchange.publish rkey, msg,
                deliveryMode: 1 # non-persistent

            return
          .catch console.error
          return
        return

      {sub,connection}

    if module is require.main
      main false
    else
      module.exports = main
